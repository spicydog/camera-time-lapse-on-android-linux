<?php

function startsWith($haystack, $needle) {
    // search backwards starting from haystack length characters from the end
    return $needle === "" || strrpos($haystack, $needle, -strlen($haystack)) !== false;
}

function endsWith($haystack, $needle) {
    // search forward starting from end minus needle length characters
    return $needle === "" || (($temp = strlen($haystack) - strlen($needle)) >= 0 && strpos($haystack, $needle, $temp) !== false);
}

$dir = ".";
$dh  = opendir($dir);
$dirs = [];
while (false !== ($filename = readdir($dh))) {
  if (endsWith($filename, '.jpg')) {
    $files[] = $filename;
  }
  if (strpos($filename, '.') === false) {
    $dirs[] = $filename;
  }
}

rsort($files);
rsort($dirs);

echo 'History';
foreach($dirs as $i => $dir) {
  if ($i===0) {
    continue;
  }
  printf(': <a href=%s/animation.gif>%s</a> ',$dir,$dir);
  if ($i>30) {
    break;
  }
}
echo '<br />';
echo '<br />';

foreach($files as $i => $filename) {
  $text = str_replace('.png','',$filename);
  $text = gmdate("Y-m-d H:i:s", intval($text)+7*60*60);
  if ($i<10) {
    printf('<img src="%s" title="%s" /><br /><br />',$filename,$text);
  } else {
    printf('<a target="_blank" href="%s">%s</a><br />',$filename,$text);
  }
  if ($i>500) {
    break;
  }

}
