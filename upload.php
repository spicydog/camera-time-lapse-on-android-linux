<?php
$target_dir = "./";
$target_file = $target_dir . basename(time().'.jpg');
$uploadOk = 1;
$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
// Check if image file is a actual image or fake image
if(isset($_POST["submit"])) {
    $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
    if($check !== false) {
        echo "File is an image - " . $check["mime"] . ".";
        $uploadOk = 1;
    } else {
        echo "File is not an image.";
        $uploadOk = 0;
    }
}
// Allow certain file formats
if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
&& $imageFileType != "gif" ) {
    echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
    $uploadOk = 0;
}
// Check if $uploadOk is set to 0 by an error
if ($uploadOk == 0) {
    echo "Sorry, your file was not uploaded.";
// if everything is ok, try to upload file
} else {
    if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
        if (isset($_POST["dt"])) {
            $dt = explode(' ', $_POST["dt"]);
            $date = $dt[0];

            if (!file_exists($date)) {
                mkdir($date, 0777, true);
            }
            $name = str_replace(' ', '', $_POST["dt"]);
            $name = str_replace('-', '', $name);
            $name = str_replace(':', '', $name);
            $target = './' . $date . '/' . $name . '.jpg';
            copy ($target_file, $target);
        }

        echo "The file ". basename( $_FILES["fileToUpload"]["name"]). " has been uploaded.";
    } else {
        echo "Sorry, there was an error uploading your file.";
    }
}

function endsWith($haystack, $needle) {
    // search forward starting from end minus needle length characters
    return $needle === "" || (($temp = strlen($haystack) - strlen($needle)) >= 0 && strpos($haystack, $needle, $temp) !== false);
}


$dir = ".";
$dh  = opendir($dir);
while (false !== ($filename = readdir($dh))) {
  if (endsWith($filename, '.jpg')) {
    $files[] = $filename;
  }
}

rsort($files);

foreach($files as $i => $filename) {
  if ($i > 500) {
    unlink($filename);
  }
}
